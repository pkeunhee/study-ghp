use jtalks;

select * from sections;
select * from users;

SELECT
  acl_object_identity.object_id_identity,
  acl_object_identity.id AS acl_id,
  acl_object_identity.parent_object,
  acl_object_identity.entries_inheriting,
  acl_entry.id AS ace_id,
  acl_entry.mask,
  acl_entry.granting,
  acl_entry.audit_success,
  acl_entry.audit_failure,
  acl_sid.principal  AS ace_principal,
  acl_sid.sid as aclsid,
  acli_sid.principal AS acl_principal,
  acli_sid.sid       AS acl_sid,
  acl_class.class
FROM
  acl_object_identity LEFT JOIN acl_sid acli_sid ON acli_sid.id = acl_object_identity.owner_sid
LEFT JOIN acl_class ON acl_class.id = acl_object_identity.object_id_class
LEFT JOIN acl_entry ON acl_object_identity.id = acl_entry.acl_object_identity
LEFT JOIN acl_sid ON acl_entry.sid = acl_sid.id
WHERE
  (
    (
      acl_object_identity.object_id_identity = 1
    AND acl_class.class                      = 'BRANCH'
    )
  )
ORDER BY
  acl_object_identity.object_id_identity ASC,
  acl_entry.ace_order ASC;
-- set serveroutput on
declare
  counter integer;
begin
  counter := counter + 1;
  if counter is null then
    dbms_output.put_line('Result : COUNTER IS NULL');
  end if;
end;

declare
  counter INTEGER;
  myResult INTEGER;
begin
  myResult := 10 / 0;
  dbms_output.put_line(myResult);
exception when others then
  dbms_output.put_line('0으로는 나눌수 없음');
  myResult := 10 / 1;
  dbms_output.put_line(myResult);
end;


declare 
  counter integer;
  i integer;
begin
  for i in 1..10 loop
    counter := (2 * i);
    dbms_output.put_line(' 2 * ' || i || ' = ' || counter);
  end loop;
end;

select * from bom

declare
  type varray1 is varray(3) of varchar2(100);
  type collection1 is table of varchar2(10);
  type map1 is table of varchar(32) index by varchar(64);
  
  names varray1;
  countries collection1;
  students map1;
begin
  names := varray1('geunhui park', 'hyejung jin');
  countries := collection1('korea', 'japan', 'china', 'usa');
  students('ghpark') := '박근희';
  
  dbms_output.put_line(names(2));
  dbms_output.put_line(countries(4));
  dbms_output.put_line(students('ghpark'));
end;


declare
  record2 bom%ROWTYPE;
  
  cursor cursor1 is 
    select item_id, parent_id, item_name, item_qty from bom;
  
  -- 커서로부터 ROWTYPE 을 얻어옴
  record3 cursor1%ROWTYPE;
  
begin
  record2.item_id := 2002;
  record2.parent_id := 2001;
  record2.item_name := 'ps3';
  record2.item_qty := 2;
  
  -- insert into bom values record2;
  
  open cursor1;
  loop
    fetch cursor1 into record3;
    dbms_output.put_line(record3.item_name);
    exit when cursor1%NOTFOUND;
  end loop;
  
exception when others then
  rollback;

end;



/*
 * @(#)AutoBoxingProblemTest.java
 *
 * Copyright 2007 NHN Corp. All rights Reserved. 
 * NHN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package kr.pe.ghp.study;

import org.junit.Test;

/**
 * @author 박근희
 */
public class AutoBoxingProblemTest {


	@Test
	public void test2() {
		String a = "abc";
		String b = new String("abc");
		String c = "abc";

		boolean r1 = a == b;
		
		System.out.println(r1);
		System.out.println(a == c);
		System.out.println(b == c);
	}
}

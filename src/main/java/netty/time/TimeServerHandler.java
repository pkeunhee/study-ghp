package netty.time;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author geunhui park
 */
public class TimeServerHandler extends ChannelInboundHandlerAdapter {
	/**
	 * channelActive : 커넥션이 맺어지고 트래픽을 발생할 준비가 되었을때 발생
	 */
	@Override
	public void channelActive(final ChannelHandlerContext ctx) throws Exception {
//		final ByteBuf time = ctx.alloc().buffer(4); // 새 버퍼를 할당 한다
//		time.writeInt((int) (System.currentTimeMillis() / 1000L + 2208988800L));
//
//		final ChannelFuture f = ctx.writeAndFlush(time); // 아직은 발생하지 않았고 미래에 발생할 I/O
//		f.addListener(new ChannelFutureListener() {
//			@Override
//			public void operationComplete(ChannelFuture future) throws Exception {
//				// 쓰기가 완료되면 close 함
//				assert f == future;
//				ctx.close();
//			}
//		});
		
		ChannelFuture f = ctx.writeAndFlush(new UnixTime());
		f.addListener(ChannelFutureListener.CLOSE);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
}

package netty.time;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * @author geunhui park
 */
public class TimeClient {
	public static void main(String[] ar) throws Exception {
		String host = "localhost";
		int port = 18080;
		EventLoopGroup workerGroup = new NioEventLoopGroup();

		try {
			Bootstrap b = new Bootstrap(); // ServerBootstrap 이 아닌 client 사이드는 Bootstrap 임
			b.group(workerGroup);
			b.channel(NioSocketChannel.class); // NioServerSocketChannel 이 아닌 client 사이드는 NioSocketChannel 임
			b.option(ChannelOption.SO_KEEPALIVE, true);
			b.handler(new ChannelInitializer<Channel>() {
				@Override
				public void initChannel(Channel ch) throws Exception {
					// ch.pipeline().addLast(new TimeClientHandler()); //단편화 이슈가 있음
					ch.pipeline().addLast(new TimeDecoder(), new TimeClientHandler()); // 단편화 이슈 해결을 위해 TimeDecoder 사용
				};
			});

			ChannelFuture f = b.connect(host, port).sync();
			f.channel().closeFuture().sync();
		} finally {
			workerGroup.shutdownGracefully();
		}
	}
}

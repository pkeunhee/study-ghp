package netty.time;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * @author geunhui park
 */
public class TimeServer {
	private int port;

	public TimeServer(int port) {
		this.port = port;
	}

	public void run() throws Exception {
		// boss 는 들어오는 커넥션을 받아들인다.
		EventLoopGroup bossGroup = new NioEventLoopGroup(); // NioEventLoopGroup : I/O 작업을 처리하는 멀티스레드의 이벤트 루프
		// worker 는 boss 가 승인한 커넥션의 트래픽을 처리
		EventLoopGroup workerGroup = new NioEventLoopGroup();

		try {
			// ServerBootstrap : 서버 설정을 도와 주는 helper class. 직접 channel 을 사용하여 설정할 수도 있지만 루틴한 작업이므로 helper 를 사용 한다. 
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup)
			.channel(NioServerSocketChannel.class) //
			.childHandler(new ChannelInitializer<SocketChannel>() {
				@Override
				protected void initChannel(SocketChannel ch) throws Exception {
					ch.pipeline().addLast(new TimeEncoder(), new TimeServerHandler()); //handler 지정

				}
			})
			.option(ChannelOption.SO_BACKLOG, 128) //TCP 옵션 지정
			.childOption(ChannelOption.SO_KEEPALIVE, true);
			ChannelFuture f = b.bind(port).sync();
			f.channel().closeFuture().sync();
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}

	public static void main(String[] ar) throws Exception {
		new TimeServer(18080).run();
	}
}

package kr.pe.ghp.study.jmx2;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

/**
 * @author geunhui park
 */
public class JMXAgent {
	private MBeanServer mbeanServer;
	private String jmxServerName = "TestJMXServer";

	public void start() throws MalformedObjectNameException, NullPointerException, InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException, IOException {
		int rmiPort = 1099;
		
		LocateRegistry.createRegistry(rmiPort);
		mbeanServer = ManagementFactory.getPlatformMBeanServer();

		JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://localhost:" + rmiPort + "/" + jmxServerName);
		System.out.println("JMXServiceURL: " + url.toString());
		JMXConnectorServer jmxConnServer = JMXConnectorServerFactory.newJMXConnectorServer(url, null, mbeanServer);
		jmxConnServer.start();

	}

	public void registerMBean(StatusMBean statusMBean) throws NotCompliantMBeanException, InstanceAlreadyExistsException, MBeanRegistrationException, MalformedObjectNameException {
		ObjectName objName = new ObjectName(jmxServerName + ":name=" + statusMBean.getName());
		mbeanServer.registerMBean(statusMBean, objName);
	}

	public static void main(String[] args) throws MalformedObjectNameException, InstanceAlreadyExistsException, NotCompliantMBeanException, MBeanRegistrationException, IOException {
		JMXAgent agent = new JMXAgent();
		agent.start();
		
		//MBean 등록하기
		agent.registerMBean(new Status("pause", "music"));
		agent.registerMBean(new Status("start", "video"));
	}
}
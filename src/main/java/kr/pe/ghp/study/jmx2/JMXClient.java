package kr.pe.ghp.study.jmx2;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import javax.management.Attribute;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

/**
 * @author geunhui park
 */
public class JMXClient {
	public void start() throws IOException, AttributeNotFoundException, MBeanException, ReflectionException, InstanceNotFoundException, InvalidAttributeValueException, MalformedObjectNameException {
		System.out.println("Create an RMI connector client and " + "connect it to the RMI connector server");
		JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://localhost:1099/TestJMXServer");
		JMXConnector jmxConnector = JMXConnectorFactory.connect(url, null);

		// 연결 생성하기
		System.out.println("Get an MBeanServerConnection");
		MBeanServerConnection mServerConn = jmxConnector.getMBeanServerConnection();

		// 도메인 목록 가져오기.
		System.out.println("Domains:");
		String domains[] = mServerConn.getDomains();
		for (int i = 0; i < domains.length; i++) {
			System.out.println("/tDomain[" + i + "] = " + domains[i]);
		}

		System.out.println("MBean count = " + mServerConn.getMBeanCount()); // MBean 개수

		System.out.println("Query MBeanServer MBeans:"); // MBean 목록
		Set<ObjectName> names = new TreeSet<ObjectName>(mServerConn.queryNames(null, null));
		for (ObjectName name : names) {
			System.out.println(name);
		}

		ObjectName stdMBeanName = new ObjectName("TestJMXServer:name=music");

		// bean 정보 가져오기
		try {
			MBeanInfo info = mServerConn.getMBeanInfo(stdMBeanName);
			System.out.println(info.toString());

			for (MBeanAttributeInfo attr : info.getAttributes()) {
				System.out.println(attr.getName());
			}
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}

		// 지정한 MBean 에서 특정 프로퍼티 정보 가져오기 (MBean 인터페이스의 메소드명 기준 임. 예: getName 은 attribute 가 Name 임)
		for (Attribute attr : mServerConn.getAttributes(stdMBeanName, new String[] { "Status", "Name" }).asList()) {
			System.out.println(attr.getValue().toString());
		}

		// proxy 를 이용하는 방법
		StatusMBean proxy = (StatusMBean) MBeanServerInvocationHandler.newProxyInstance(mServerConn, stdMBeanName, StatusMBean.class, false);
		System.out.println(proxy.getName());
		proxy.setName("another name");
		System.out.println(proxy.getName());

		System.out.println("Close the connection to the server");
		jmxConnector.close(); // 연결 끊기
	}

	public static void main(String[] args)
			throws IOException, AttributeNotFoundException, MBeanException, ReflectionException, InstanceNotFoundException, InvalidAttributeValueException, MalformedObjectNameException {
		new JMXClient().start();
	}
}
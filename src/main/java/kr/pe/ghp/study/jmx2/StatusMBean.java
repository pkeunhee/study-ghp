package kr.pe.ghp.study.jmx2;

/**
 * MBean 스펙 인터페이스
 * 
 * @author geunhui park
 */
public interface StatusMBean {
	public String getName();

	public void setName(String name);

	public String getStatus();

	public void setStatus(String status);
}

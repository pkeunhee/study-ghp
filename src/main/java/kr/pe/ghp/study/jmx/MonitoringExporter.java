package kr.pe.ghp.study.jmx;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

/**
 * MBean server 를 생성하고
 * MBean 을 register 하기 위함
 * 
 * @author khpark
 */
public class MonitoringExporter {
	private Monitoring monitoring;

	public MonitoringExporter() {
		try {
			this.monitoring = new Monitoring();

			MBeanServer server = ManagementFactory.getPlatformMBeanServer();
			// new ObjectName("임의의 MBean 명 :type=임의의 Type명");
			ObjectName jmxObjectName = new ObjectName("SomeApp:type=SomeType"); // 여기서 정의된 MBean 이름이 프로파일링 툴 (visual vm 등) 에서 리스팅 된다.
			server.registerMBean(monitoring, jmxObjectName); //객체:ObjectName 으로 등록. ObjectName 으로 client 에서 정보 요청할 수 있다.

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * -Dcom.sun.management.jmxremote.port=9999(임의의 포트 - 사용자 설정)
	 * -Dcom.sun.management.jmxremote.ssl=false
	 * -Dcom.sun.management.jmxremote.authenticate=false
	 *
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		MonitoringExporter exporter = new MonitoringExporter();
	}

}

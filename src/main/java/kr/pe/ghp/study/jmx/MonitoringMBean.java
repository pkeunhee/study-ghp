package kr.pe.ghp.study.jmx;

public interface MonitoringMBean {
    long getExecuteTime();
    long getPollingCount();
    long getChannelPercentage();
}

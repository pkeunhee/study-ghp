package kr.pe.ghp.study.jmx;

/**
 * 프로파일링 데이터 샘플 제공하기 위한 목적
 * 
 * @author geunhui park
 */
public class MonitoringCounter {
	private static MonitoringCounter counter;

	private long executeTime;
	private long pollingCount;
	private long channelPercentage;

	private MonitoringCounter() {
	}

	public static MonitoringCounter getInstance() {
		if (counter == null) {
			counter = new MonitoringCounter();
		}
		return counter;
	}

	public long getExecuteTime() {
		return executeTime;
	}

	public void setExecuteTime(long executeTime) {
		this.executeTime = executeTime;
	}

	public long getPollingCount() {
		return pollingCount;
	}

	public void setPollingCount(long pollingCount) {
		this.pollingCount = pollingCount;
	}

	public long getChannelPercentage() {
		return channelPercentage;
	}

	public void setChannelPercentage(long channelPercentage) {
		this.channelPercentage = channelPercentage;
	}

}

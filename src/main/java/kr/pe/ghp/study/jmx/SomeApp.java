package kr.pe.ghp.study.jmx;

import java.util.Random;

public class SomeApp {
	private Random rand;

	public SomeApp() {
		this.rand = new Random();
	}

	public void run() {
		System.out.println("do something.....");
		MonitoringCounter.getInstance().setChannelPercentage(rand.nextLong());
		MonitoringCounter.getInstance().setExecuteTime(rand.nextLong());
		MonitoringCounter.getInstance().setPollingCount(rand.nextLong());
	}

	/**
	 * 실행시 옵션
	 * -Dcom.sun.management.jmxremote.port=9999
	 * -Dcom.sun.management.jmxremote.ssl=false
	 * -Dcom.sun.management.jmxremote.authenticate=false
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		MonitoringExporter.main(null); // APP 이 실행할때 MBean 등록해 주는 로직을 수행한다.
		SomeApp someApp = new SomeApp();
		while (true) {
			someApp.run();
			Thread.sleep(1000);
		}
	}
}

package kr.pe.ghp.study.jmx;

/**
 * MBean 스펙으로 만들어짐.
 * 프로파일링 정보를 제공 함
 * 
 * @author geunhui park
 */
public class Monitoring implements MonitoringMBean {
	public long getExecuteTime() {
		return MonitoringCounter.getInstance().getExecuteTime();
	}

	public long getPollingCount() {
		return MonitoringCounter.getInstance().getPollingCount();
	}

	public long getChannelPercentage() {
		return MonitoringCounter.getInstance().getChannelPercentage();
	}
}

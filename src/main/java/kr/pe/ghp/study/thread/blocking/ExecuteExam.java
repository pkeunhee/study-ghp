package kr.pe.ghp.study.thread.blocking;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ExecuteExam {
	public static void main(String[] args) throws Exception {
		final ExecutorService executorService = Executors.newFixedThreadPool(2);

		for (int i = 0; i < 10; i++) {
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) executorService;

					int poolSize = threadPoolExecutor.getPoolSize();
					String threadName = Thread.currentThread().getName();
					System.out.println("[총 스레드 개수: " + poolSize + "] 작업 스레드 이름: " + threadName);

					int value = Integer.parseInt("숫자");
				}
			};

			// executorService.execute(runnable); // 작업 처리중 예외가 발생하면 해당 스레드는 제거되고 새 스레드를 생성한다.
			executorService.submit(runnable); // 작업 처리중 예외가 발생하면 해당 스레드는 종료되지 않고 재사용되어 다른 작업을 처리 한다.

			Thread.sleep(10);
		}

		executorService.shutdown();
	}
}
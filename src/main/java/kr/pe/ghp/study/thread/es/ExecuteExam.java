package kr.pe.ghp.study.thread.es;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ExecuteExam {
	public static void main(String[] ar) throws Exception {
		final ExecutorService executorService = Executors.newFixedThreadPool(2);

		for (int i = 0; i < 10; i++) {
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) executorService;
					int poolSize = threadPoolExecutor.getPoolSize();
					String threadName = Thread.currentThread().getName();
					System.out.println("[총 스레드 개수: " + poolSize + "] / 작업 스레드 이름: " + threadName);

					int value = Integer.parseInt("문자");
				}
			};

			// executorService.execute(runnable); //execute 로 스레드를 등록한 경우 Exception 이 발생하면, 해당 스레드를 풀에서 제거하고 새로운 스레드를 생성 시킨다.
			executorService.submit(runnable); // submit 으로 스레드를 등록한 경우 Exception 이 발생하면, 해당 스레드를 풀에서 제거 하진 않고 다른 일을 하도록 한다.
			Thread.sleep(10);
		}

		executorService.shutdown();
	}
}

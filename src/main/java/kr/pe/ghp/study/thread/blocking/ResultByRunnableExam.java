package kr.pe.ghp.study.thread.blocking;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ResultByRunnableExam {
	public static void main(String[] args) {
		ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

		System.out.println("작업 처리 요청 !");

		class Task implements Runnable {
			Result result;

			Task(Result result) {
				this.result = result;
			}

			@Override
			public void run() {
				int sum = 0;
				for (int i = 1; i <= 10; i++) { // 1~10 의 합 = 55
					sum += i;
				}

				result.addValue(sum);
			}
		}
		;

		// 스레드에서 처리한 결과값을 누적 시키는 외부 객체 이용
		Result result = new Result();
		Runnable task1 = new Task(result); // 생성자로 ref 주입
		Runnable task2 = new Task(result);
		Future<Result> future1 = executorService.submit(task1, result);
		Future<Result> future2 = executorService.submit(task2, result);

		try {
			result = future1.get();
			result = future2.get();
			System.out.println("처리 결과: " + result.accumValue);
			System.out.println("처리 완료");
		} catch (Exception e) {
			e.printStackTrace();
		}

		executorService.shutdown();
	}
}

class Result {
	int accumValue;

	synchronized void addValue(int value) {
		accumValue += value;
	}
}
package kr.pe.ghp.study.thread.sync;

class SyncUser {
	private int userNo = 0;

	// 임계 영역을 지정하는 synchronized 메소드
	public synchronized void add(String name) {
		System.out.println(name + " : " + userNo++ + "번째 사용");
	}
}

class SyncUserThread extends Thread {
	SyncUser user;

	SyncUserThread(SyncUser user, String name) {
		super(name); // 스레드의 이름 세팅
		this.user = user;
	}

	public void run() {
		try {
			for (int i = 0; i < 3; i++) {
				user.add(getName());
				sleep(500);
			}
		} catch (InterruptedException e) {
			System.err.println(e.getMessage());
		}
	}
}

public class SyncThread {
	public static void main(String[] args) {
		SyncUser user = new SyncUser(); // 여러 스레드에 공유되는 객체

		// 3개의 스레드 객체 생성
		SyncUserThread p1 = new SyncUserThread(user, "A1");
		SyncUserThread p2 = new SyncUserThread(user, "B2");
		SyncUserThread p3 = new SyncUserThread(user, "C3");

		// 스레드 스케줄링 : 우선순위 부여
		p1.setPriority(p1.MAX_PRIORITY);
		p2.setPriority(p2.NORM_PRIORITY);
		p3.setPriority(p3.MIN_PRIORITY);

		System.out.println("-----------------------");
		System.out.println("sychronized 적용안한 경우");
		System.out.println("-----------------------");

		// 스레드 시작
		p1.start();
		p2.start();
		p3.start();
	}
}
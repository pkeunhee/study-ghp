package kr.pe.ghp.study.socket;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class TcpClient {
	public static void main(String[] args) {
		try {
			String serverIP = "10.0.0.10"; // 127.0.0.1 & localhost 본인
			int port = 11001;
			//int port = 5001;
			System.out.println("서버에 연결중입니다. 서버 IP : " + serverIP);

			String trNo = StringUtils.substring(String.valueOf(System.currentTimeMillis()), 1); //거래번호
			String amt = StringUtils.leftPad("1200", 13, "0") ;			
			String bankCd = "04";
			String acctno = StringUtils.rightPad("45459014434381", 16, " ");
			
			StringBuffer ft = new StringBuffer();
			ft.append("VR");
			ft.append(trNo); //12. 거래번호
			ft.append(StringUtils.rightPad("KSNETVR", 9, " ")); //식별코드
			ft.append("20000015");
			ft.append(bankCd); //국민은행
			ft.append("0200"); //메세지코드
			ft.append("300"); //업무구분
			ft.append("1"); //송신횟수
			ft.append("003394"); //전문번호
			ft.append(new SimpleDateFormat("yyyyMMdd").format(new Date())); //전송일자
			ft.append(new SimpleDateFormat("HHmmss").format(new Date())); //전송시간
			ft.append("0000"); //응답코드
			ft.append("    "); //은행응답코드
			ft.append("        "); //조회일자
			ft.append("      "); //조회번호
			ft.append("                               "); //예비
			ft.append("               "); //계좌번호 = 모계좌번호
			ft.append("01"); //조립건수
			ft.append("20"); //거래구분
			ft.append("11"); //은행코드
			ft.append(amt); //금액
			ft.append("0000000000000"); //잔액
			ft.append("010101"); //입금점지로코드
			ft.append("전지혜        "); //14. 입금인 성명
			ft.append("          "); //수표번호
			ft.append("0000000000000"); //현금(현금_당좌수표)
			ft.append("0000000000000"); //타행수표금액
			ft.append("0000000000000"); //가계수표, 기타
			ft.append(acctno); //16. 가상계좌번호
			ft.append(new SimpleDateFormat("yyyyMMdd").format(new Date())); //거래일자
			ft.append(new SimpleDateFormat("HHmmss").format(new Date())); //거래시간
			ft.append("      "); //통장거래일련번호
			ft.append("                                                "); //예비
			
			String ft2 = "VR610840000002KSNETVR  200000152602003001003394200301021406020000                                                                01201100000000408000000000000000010101박찬                    00000000000000000000000000000000000000028690100000687  20021001105618                                                      ";
			
			// 소켓을 생성하여 연결을 요청한다.
			Socket socket = new Socket(serverIP, port);
			//Socket socket = new Socket(serverIP, 5001);
			socket.setSoTimeout(9999999);

			
			//String ft = "   1. Definitions.      \"License\" shall mean the terms and conditions for use, reproduction,      and distribution as defined by Sections 1 through 9 of this document.      \"Licensor\" shall mean the copyright owner or entity authorized by      the copyright ownerthat is granting the License.      \"Legal Entity\" shall mean the union of the acting entity and all      other entities that control, are controlled by, or are under common      control with that entity. For the purposes of this definition,      \"control\" means(i)the power, direct or indirect, to cause the      direction or management of such entity, whether by contract or      otherwise, or (ii) ownership of fifty percent (50%) or more of the      outstanding shares, or (iii) beneficial ownership of such entity.      \"You\" (or \"Your\") shall mean an individual or Legal Entity      exercising permissions granted by this License.      \"Source\" form shall mean the preferred form for making modifications,      including but not limited to software source code,documentation      source, and configuration files.      \"Object\" form shall mean any form resulting from mechanical      transformation or translation of a Source form, including but      not limited to compiled object code, generated documentation,      and conversions to other media types.      \"Work\" shall mean the work of authorship, whether in Source or      Object form, made available under the License, as indicated by a      copyright notice that is included in or attached to the work      (an example is provided in the Appendix below).      \"Derivative Works\" shall mean any work, whether in Source or Object      form, that is based on (or derived from) the Work and for which the      editorial revisions, annotations, elaborations, orother modifications      represent, as a whole, an original work of authorship. For the purposes      of this License, Derivative Works shall not include works that remain      separable from, or merely link (or bind by name) to the interfaces of,      the Workand Derivative Works thereof.      \"Contribution\" shall mean any work of authorship, including      the original version of the Work and any modifications or additions      to that Work or Derivative Works thereof, that is intentionally      submitted toLicensor for inclusion in the Work by the copyright owner      or by an individual or Legal Entity authorized to submit on behalf of      the copyright owner. For the purposes of this definition, \"submitted\"      means any form of electronic, verbal, or writtencommunication sent      to the Licensor or its representatives, including but not limited to      communication on electronic mailing lists, source code control systems,      and issue tracking systems that are managed by, or on behalf of, the      Licensorfor the purpose of discussing and improving the Work, but      excluding communication that is conspicuously marked or otherwise      designated in writing by the copyright owner as \"Not a Contribution.\"      \"Contributor\" shall mean Licensor andany individual or Legal Entity      on behalf of whom a Contribution has been received by Licensor and      subsequently incorporated within the Work.   2. Grant of Copyright License. Subject to the terms and conditions of      this License, eachContributor hereby grants to You a perpetual,      worldwide, non-exclusive, no-charge, royalty-free, irrevocable      copyright license to reproduce, prepare Derivative Works of,      publicly display, publicly perform, sublicense, and distribute the      Workand such Derivative Works in Source or Object form.   3. Grant of Patent License. Subject to the terms and conditions of      this License, each Contributor hereby grants to You a perpetual,      worldwide, non-exclusive, no-charge, royalty-free,irrevocable      (except as stated in this section) patent license to make, have made,      use, offer to sell, sell, import, and otherwise transfer the Work,      where such license applies only to those patent claims licensable      by such Contributorthat are necessarily infringed by their      Contribution(s) alone or by combination of their Contribution(s)      with the Work to which such Contribution(s) was submitted. If You      institute patent litigation against any entity (including a     cross-claim or counterclaim in a lawsuit) alleging that the Work      or a Contribution incorporated within the Work constitutes direct      or contributory patent infringement, then any patent licenses      granted to You under this License for that Work shallterminate      as of the date such litigation is filed.   4. Redistribution. You may reproduce and distribute copies of the      Work or Derivative Works thereof in any medium, with or without      modifications, and in Source or Object form, provided thatYou      meet the following conditions:      (a) You must give any other recipients of the Work or          Derivative Works a copy of this License; and      (b) You must cause any modified files to carry prominent notices          stating that You changed thefiles; and      (c) You must retain, in the Source form of any Derivative Works          that You distribute, all copyright, patent, trademark, and          attribution notices from the Source form of the Work,          excluding those notices that do not pertaintoany part of          the Derivative Works; and      (d) If the Work includes a \"NOTICE\" text file as part of its          distribution, then any Derivative Works that You distribute must          include a readable copy of the attribution notices contained         within such NOTICE file, excluding those notices that do not          pertain to any part of the Derivative Works, in at least one          of the following places: within a NOTICE text file distributed          as part of the Derivative Works; within the Source form or          documentation, if provided along with the Derivative Works; or,          within a display generated by the Derivative Works, if and          wherever such third-party notices normally appear. The contents          of the NOTICE file are for informationalpurposes only and          do not modify the License. You may add Your own attribution          notices within Derivative Works that You distribute, alongside";
			socket.getOutputStream().write(ft.toString().getBytes("EUC-KR"));
			
			// 소켓의 입력스트림을 얻는다.
			InputStream in = socket.getInputStream();
			DataInputStream dis = new DataInputStream(in); // 기본형 단위로 처리하는 보조스트림

			// 소켓으로 부터 받은 데이터를 출력한다.
			System.out.println("서버로부터 받은 메세지 : " + dis.readUTF());
			System.out.println("연결을 종료합니다.");

			// 스트림과 소켓을 닫는다.
			dis.close();
			socket.close();
		} catch (ConnectException ce) {
			ce.printStackTrace();
		} catch (IOException ie) {
			ie.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

import java.util.Map;

import org.apache.commons.codec.binary.StringUtils;
import org.junit.Test;

import com.google.common.collect.Maps;

public class CodeGenerator {
	@Test
	public void suitepayment() {
		Map<String, String> map = Maps.newLinkedHashMap();
		map.put("INPAYSEQ", "1481822745");
		map.put("INCHNL", "VA");
		map.put("INUSERKEY", "4f69a900-2bad-11e6-850c-000000000e4d");
		map.put("INPGID", "32001");
		map.put("INCPID", "Vcoin_WEB");
		map.put("INITEMID", "vcoin_50_pc");
		map.put("INPAYAMT", "1100");
		map.put("INCURRENCY", "KRW");
		map.put("INLOCATION", "''");
		map.put("INTITLE", "50 COIN");
		map.put("INMEMO", "''");
		map.put("INMGRID", "''");
		map.put("INREMOTEIP", "10.64.88.178");
		map.put("INCORPCD", "Alipay");
		map.put("INENCPAYNO", "''");
		map.put("INRSVNO", "1116.1");
		map.put("INAUTHNO", "2017082921001003840200490500");
		map.put("INSUBNO", "d6d15db46a23cae486eba2f5bbd9474b");
		map.put("INSUBSFG", "N");
		map.put("INDIRECTFG", "''");
		map.put("ININSTMO", "''");
		map.put("INPOINTFG", "''");
		map.put("INAUTHDTTM", "''");
		map.put("INSVCINF", "''");
		map.put("INSETTLERCTDT", "''");

		for (String key : map.keySet()) {
			boolean isNull = org.apache.commons.lang3.StringUtils.isBlank(map.get(key));

			String value = null;
			if (isNull) {
				value = String.format("%s := NULL;", key);
			} else if ("''".equals(map.get(key))) {
				value = String.format("%s := '';", key);
			} else {
				value = String.format("%s := '%s';", key, map.get(key));
			}

			System.out.println(value);
		}
	}
}

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
	
	<script type="text/javascript">
		var person = {
			name: 'default name',
			hello: function(){
				alert('Hello My name is ' + this.name);
			}
		};
		
		
		function Ghp(){
			this.name = 'geunhui park';
		};
		
		Ghp.prototype = person;
		var geunhuipark = new Ghp();
		geunhuipark.hello();
		//person.hello();
		
		
	
	</script>
</head>
<body>
<h1>
	Hello world!  
</h1>

<P>  The time on the server is ${serverTime}. </P>
</body>
</html>

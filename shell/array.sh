#!/bin/bash
names=("geunhui park" "hj jin")

echo ${#names[@]} #배열의 크기
echo ${#names[0]} #0번 element의 크기 출력. (글자수)
echo ${!names[*]} #인덱스 번호 출력
echo ${names[0]} #배열 element에 접근
echo ${names[*]}

echo "반복문"
# 반복문
for aName in "${names[@]}"; do
	echo $aName
done

# 반복문
for ((i=1;i<=10;i++)); do
	echo "$i"
done

echo "I am `whoami`"